from django.contrib import admin
from django.urls import path
from .views import playlist_json, video_json, new_playlist_json
urlpatterns = [
    path('playlist/<int:id>', playlist_json),
    path('new_playlist/<str:playlist_id>', new_playlist_json),
    path('video/<str:id>', video_json),
]
