from django.shortcuts import render
import datetime
import pafy
from django.http import JsonResponse
from .models import Playlist, Video
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
@csrf_exempt
def playlist_json(request, id):
    """
    Get a json with details of each video
    """
    playlist = Playlist.objects.get(pk=id)
    if (datetime.datetime.now(datetime.timezone.utc) - playlist.expiry).seconds > 6*3600:
        playlist.save()
    data = []
    videos = Video.objects.filter(playlist=id)
    for v in videos:
        data.append({
            "url": v.audiourl,
            "title": v.title,
            "thumb": v.thumbnail
        }) 
    return JsonResponse(pla, safe=False)

@csrf_exempt
def new_playlist_json(request, playlist_id):
    """
    Get a json with details of each video
    """
    playlist_url = "https://www.youtube.com/watch?list=" + playlist_id
    playlist = pafy.get_playlist(playlist_url)
    vids = [
        {"video_id": vp['pafy'].videoid, 
            "title": vp['pafy'].title,
            "thumb": vp['pafy'].thumb} for vp in playlist["items"]
    ]
    """
    if Playlist.objects.filter(url = playlist_url).exists():
        playlist = Playlist.objects.get(url = playlist_url)
    else:
        playlist = Playlist(
            url="https://www.youtube.com/watch?list=" + playlist_id
        )

    playlist.save()
    videos = Video.objects.filter(playlist=playlist.pk)
    
    videos = playlist["items"]
    data = []
    for v in videos:
        vp = v["pafy"]
        data.append({
            "video_id": vp.videoid, 
            "title": vp.title,
            "thumb": vp.bigthumbhd
        }) 
        """
    return JsonResponse(vids, safe=False)

@csrf_exempt
def video_json(request, id):
    video = pafy.new(id)
    data = {
        "vid_id":id,
        "title" : video.title,
        "thumbnail": video.thumb
    }
    if video.bigthumbhd:
        data["thumbnail"] = video.bigthumbhd
    elif video.bigthumb:
        data["thumbnail"] = video.bigthumb
    
    for ast in video.audiostreams:
        if ast.extension == 'm4a': 
            data["audio_url"] = ast.url
            break
    return JsonResponse(data, safe=False)