import datetime
from django.db import models
import pafy

# Create your models here.
class Playlist(models.Model):
    """(Playlist description)"""
    title = models.CharField(blank=True, max_length=100)
    url = models.CharField(blank=True, max_length=1000)
    expiry = models.DateTimeField(blank=True, default=datetime.datetime.now)
    
    def save(self, *args, **kwargs):
        super(Playlist, self).save(*args, **kwargs)
        print(self.url, self.pk)
        self.fetch_playlist()
        super(Playlist, self).save(*args, **kwargs)
    
    def fetch_playlist(self):
        """
        Fetching the audio streams takes a lot of time
        -- the call v["pafy"].audiostreams
        """
        playlist = pafy.get_playlist(self.url)
        videos = playlist["items"]
        self.title = playlist["title"]
        self.expiry = datetime.datetime.now()
        for v in videos:
            if Video.objects.filter(playlist=self, url=v["pafy"].watchv_url).exists():
                video = Video.objects.filter(playlist=self, url=v["pafy"].watchv_url)[0]
            else:
                video = Video(playlist=self, url=v["pafy"].watchv_url)
            video.title = v["pafy"].title
            video.thumbnail = v["pafy"].thumb
            if v["pafy"].bigthumbhd:
                video.thumbnail = v["pafy"].bigthumbhd
            elif v["pafy"].bigthumb:
                video.thumbnail = v["pafy"].bigthumb
            for ast in v["pafy"].audiostreams:
                if ast.extension == 'm4a': 
                    video.audiourl = ast.url
                    video.save()
                    break
    
    def __str__(self):
        return self.title

class Video(models.Model):
    """(Video description)"""
    playlist = models.ForeignKey('Playlist', on_delete=models.CASCADE)
    title = models.CharField(blank=True, max_length=100)
    url = models.CharField(blank=True, max_length=2000)
    audiourl = models.CharField(blank=True, max_length=2000)
    thumbnail = models.CharField(blank=True, max_length=100)
    def __str__(self):
        return self.title
